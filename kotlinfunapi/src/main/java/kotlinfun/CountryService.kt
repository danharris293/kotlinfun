package kotlinfun

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CountryService @Autowired constructor(val countryRepository: CountryRepository) {

    fun getCountries(name: String? = null, lang: String? = null) =
            countryRepository.getCountries(name = name, lang = lang)
}