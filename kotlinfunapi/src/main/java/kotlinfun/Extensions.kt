package kotlinfun

fun String?.nullEmptyOrInIgnoringCase(other: String?) =
        this == null || this == "" || other != null && other.contains(this, ignoreCase = true)