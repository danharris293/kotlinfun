package kotlinfun

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Repository

@Repository
class CountryRepository @Autowired constructor(val jdbcTemplate: JdbcTemplate) {

    fun getCountries(name: String? = null, lang: String? = null) =
            jdbcTemplate.query("SELECT * FROM COUNTRIES", mapCountry)
                    .filter { it.name.contains(name ?: "", ignoreCase = true) }
                    .filter { lang.nullEmptyOrInIgnoringCase(it.language) }
}
