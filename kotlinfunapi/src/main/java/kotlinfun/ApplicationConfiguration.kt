package kotlinfun

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.sql.DataSource

@Configuration
class ApplicationConfiguration

@Bean
@ConfigurationProperties(prefix = "spring.datasource")
fun dataSource(): DataSource = DataSourceBuilder.create().build()

