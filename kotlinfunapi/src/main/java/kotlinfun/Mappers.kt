package kotlinfun

import java.sql.ResultSet

var mapCountry = { rs: ResultSet?, _: Int ->
    Country(
            name = rs!!.getString("name"),
            population = rs.getString("population")?.toIntOrNull(),
            language = rs.getString("language"),
            currency = rs.getString("currency")
    )
}