package kotlinfun

data class Country(
        val name: String,
        val population: Int? = null,
        val language: String? = null,
        val currency: String? = null
)