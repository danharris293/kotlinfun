package kotlinfun

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class CountryController @Autowired constructor(val countryService: CountryService){

    @RequestMapping("/countries")
    fun getCountries(
            @RequestParam(required = false, name = "name") name: String? = null,
            @RequestParam(required = false, name = "lang") lang: String? = null) =

            mapOf("countries" to countryService.getCountries(name = name, lang = lang))
}