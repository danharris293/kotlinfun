INSERT INTO countries VALUES (1, 'Australia', 23780000, 'English', 'AUD');
INSERT INTO countries VALUES (2, 'Cambodia', 15580000, 'Khmer', 'KHR');
INSERT INTO countries VALUES (3, 'Laos', 6802000, 'Lao', 'LAK');
INSERT INTO countries VALUES (4, 'Malaysia', 30330000, 'Malaysian', 'MYR');
INSERT INTO countries VALUES (5, 'New Zealand', 4596000, 'English', 'NZD');
INSERT INTO countries VALUES (6, 'Thailand', 67960000, 'Thai', 'THB');
INSERT INTO countries VALUES (7, 'USA', NULL, NULL, NULL);
INSERT INTO countries VALUES (8, 'Vietnam', 91700000, 'Vietnamese', 'VND');