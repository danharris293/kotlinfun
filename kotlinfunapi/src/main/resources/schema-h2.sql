CREATE TABLE IF NOT EXISTS countries(id INT PRIMARY KEY, name VARCHAR(255) NOT NULL, population INT, language VARCHAR(255), currency VARCHAR(16))
