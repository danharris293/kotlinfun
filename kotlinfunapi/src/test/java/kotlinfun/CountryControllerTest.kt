package kotlinfun

import org.hamcrest.Matchers.`is`
import org.junit.Assert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.doReturn
import org.mockito.runners.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CountryControllerTest {

    @InjectMocks
    private lateinit var controller: CountryController

    @Mock
    private lateinit var countryService: CountryService

    @Test
    fun getCountriesReturnsAllCountries() {
        val expectedCountries = listOf(Country(name = "x"))
        doReturn(expectedCountries).`when`(countryService).getCountries()

        assertThat(controller.getCountries(), `is`(mapOf(Pair("countries", expectedCountries))))
    }

    @Test
    fun getCountriesWithMatchingNameReturnsCountries() {
        val expectedCountries = listOf(Country("xx"))
        doReturn(expectedCountries).`when`(countryService).getCountries(name = "x")

        assertThat(controller.getCountries(name = "x"), `is`(mapOf(Pair("countries", expectedCountries))))
    }

    @Test
    fun getCountriesWithMatchingLanguageReturnsCountries() {
        val expectedCountries = listOf(Country("xx"))
        doReturn(expectedCountries).`when`(countryService).getCountries(lang = "x")

        assertThat(controller.getCountries(lang = "x"), `is`(mapOf(Pair("countries", expectedCountries))))
    }
}