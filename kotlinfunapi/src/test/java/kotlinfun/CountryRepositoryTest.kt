package kotlinfun

import org.hamcrest.Matchers.`is`
import org.junit.Assert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.runners.MockitoJUnitRunner
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper

@RunWith(MockitoJUnitRunner::class)
class CountryRepositoryTest {

    @InjectMocks
    private lateinit var repository: CountryRepository

    @Mock
    private lateinit var jdbcTemplate: JdbcTemplate

    @Test
    fun getCountriesReturnsAllCountries() {
        val expectedCountries = listOf(Country("x"), Country("y"))
        doReturn(expectedCountries)
                .`when`(jdbcTemplate).query(anyString(), any(RowMapper::class.java))

        assertThat(repository.getCountries(), `is`(expectedCountries))
    }

    @Test
    fun getCountriesWithMatchingNameReturnsCountries() {
        val expectedCountry = Country("x")
        doReturn(listOf(expectedCountry, Country("y")))
                .`when`(jdbcTemplate).query(anyString(), any(RowMapper::class.java))

        assertThat(repository.getCountries(name = "x"), `is`(listOf(expectedCountry)))
    }

    @Test
    fun getCountriesWithPartialMatchingNameReturnsCountries() {
        val expectedCountries = listOf(Country("xx"))
        doReturn(expectedCountries)
                .`when`(jdbcTemplate).query(anyString(), any(RowMapper::class.java))

        assertThat(repository.getCountries(name = "x"), `is`(expectedCountries))
    }

    @Test
    fun getCountriesMatchingNameIgnoresCase() {
        val expectedCountries = listOf(Country("x"))
        doReturn(expectedCountries)
                .`when`(jdbcTemplate).query(anyString(), any(RowMapper::class.java))

        assertThat(repository.getCountries(name = "X"), `is`(expectedCountries))
    }

    @Test
    fun getCountriesReturnsEmptyListIfNoneMatchName() {
        doReturn(listOf(Country("x"), Country("y")))
                .`when`(jdbcTemplate).query(anyString(), any(RowMapper::class.java))

        assertThat(repository.getCountries(name = "xy"), `is`(emptyList()))
    }

    @Test
    fun getCountriesWithMatchingLanguageReturnsCountries() {
        val expectedCountry = Country(name = "x", language = "a")
        doReturn(listOf(expectedCountry, Country(name = "y", language = "b")))
                .`when`(jdbcTemplate).query(anyString(), any(RowMapper::class.java))

        assertThat(repository.getCountries(lang = "a"), `is`(listOf(expectedCountry)))
    }

    @Test
    fun getCountriesWithPartialMatchingLanguageReturnsCountries() {
        val expectedCountries = listOf(Country(name = "x", language = "aa"))
        doReturn(expectedCountries)
                .`when`(jdbcTemplate).query(anyString(), any(RowMapper::class.java))

        assertThat(repository.getCountries(lang = "a"), `is`(expectedCountries))
    }

    @Test
    fun getCountriesMatchingLanguageIgnoresCase() {
        val expectedCountries = listOf(Country(name = "x", language = "a"))
        doReturn(expectedCountries)
                .`when`(jdbcTemplate).query(anyString(), any(RowMapper::class.java))

        assertThat(repository.getCountries(lang = "A"), `is`(expectedCountries))
    }

    @Test
    fun getCountriesReturnsEmptyListIfNoneMatchLanguage() {
        doReturn(listOf(Country(name = "x", language = "a"), Country(name = "y", language = "b")))
                .`when`(jdbcTemplate).query(anyString(), any(RowMapper::class.java))

        assertThat(repository.getCountries(lang = "ab"), `is`(emptyList()))
    }

    @Test
    fun getCountriesWithEmptyLanguageReturnsAllCountries() {
        val expectedCountries = listOf(Country(name = "x", language = "a"), Country(name = "y", language = "b"))
        doReturn(expectedCountries)
                .`when`(jdbcTemplate).query(anyString(), any(RowMapper::class.java))

        assertThat(repository.getCountries(lang = ""), `is`(expectedCountries))
    }
}