package kotlinfun

import org.hamcrest.Matchers.`is`
import org.junit.Assert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.doReturn
import org.mockito.runners.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CountryServiceTest {

    @InjectMocks
    private lateinit var service: CountryService

    @Mock
    private lateinit var countryRepository: CountryRepository

    @Test
    fun getCountriesReturnsAllCountries() {
        val expectedCountries = listOf(Country("x"))
        doReturn(expectedCountries).`when`(countryRepository).getCountries()

        assertThat(service.getCountries(), `is`(expectedCountries))
    }

    @Test
    fun getCountriesWithMatchingNameReturnsCountries() {
        val expectedCountries = listOf(Country("xx"))
        doReturn(expectedCountries).`when`(countryRepository).getCountries(name = "x")

        assertThat(service.getCountries(name = "x"), `is`(expectedCountries))
    }

    @Test
    fun getCountriesWithMatchingLanguageReturnsCountries() {
        val expectedCountries = listOf(Country("xx"))
        doReturn(expectedCountries).`when`(countryRepository).getCountries(lang = "x")

        assertThat(service.getCountries(lang = "x"), `is`(expectedCountries))
    }
}