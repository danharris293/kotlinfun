package kotlinfun

import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.not
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class CountryIT {

    @Autowired
    private lateinit var mvc: MockMvc

    @Test
    fun getCountriesReturnsCountries() {
        mvc.perform(MockMvcRequestBuilders.get("/countries").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andExpect(jsonPath("countries", `is`(not(emptyList<String>()))))
    }

    @Test
    fun searchCountriesByNameReturnsCountries() {
        mvc.perform(MockMvcRequestBuilders.get("/countries").param("name", "Au").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andExpect(jsonPath("countries", `is`(not(emptyList<String>()))))
    }

    @Test
    fun searchCountriesByLanguageReturnsCountries() {
        mvc.perform(MockMvcRequestBuilders.get("/countries").param("lang", "En").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andExpect(jsonPath("countries", `is`(not(emptyList<String>()))))
    }
}