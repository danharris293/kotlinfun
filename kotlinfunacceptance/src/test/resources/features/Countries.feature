Feature: Countries API
  Users can get country information using the country API.

  Scenario: Can get all countries
    Given the system has the following countries
      | Name      | Population | Language | Currency |
      | Australia | 23780000   | English  | AUD      |
      | Cambodia  | 15580000   | Khmer    | KHR      |
      | Laos      | 6802000    | Lao      | LAK      |
    When the client requests to get all countries
    Then the response contains the following countries
      | Name      | Population | Language | Currency |
      | Australia | 23780000   | English  | AUD      |
      | Cambodia  | 15580000   | Khmer    | KHR      |
      | Laos      | 6802000    | Lao      | LAK      |
