package kotlinfun

import cucumber.api.java.Before
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate

class Hooks @Autowired constructor(val jdbcTemplate: JdbcTemplate) {

    @Before
    fun databaseTeardown() = jdbcTemplate.execute("DELETE FROM COUNTRIES")
}