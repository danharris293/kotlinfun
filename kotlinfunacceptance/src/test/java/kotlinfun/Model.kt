package kotlinfun

data class CountryModel(
        val name: String,
        val population: Int? = null,
        val language: String? = null,
        val currency: String? = null
)