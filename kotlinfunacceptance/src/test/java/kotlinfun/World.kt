package kotlinfun

import org.springframework.stereotype.Component

@Component
class World {

    var response: Any? = null

    val countries: Map<Int, CountryModel> = HashMap()
}

@Suppress("UNCHECKED_CAST")
inline fun <reified T> World.responseAsListOf() : List<T>? =
        if (response is List<*> && (response as List<*>).first() is T)
            response as List<T>
        else
            throw Exception("Response was not of correct type.")