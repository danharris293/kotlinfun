package kotlinfun

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.apache.http.client.HttpClient
import org.apache.http.impl.client.HttpClients
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class Configuration {

    @Bean
    fun httpClient(): HttpClient = HttpClients.createDefault()

    @Bean
    fun objectMapper(): ObjectMapper = jacksonObjectMapper()
}