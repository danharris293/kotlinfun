package kotlinfun

import cucumber.api.DataTable
import cucumber.api.java8.En
import org.hamcrest.Matchers.`is`
import org.junit.Assert.assertThat
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootContextLoader
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration

@ContextConfiguration(classes = arrayOf(Application::class), loader = SpringBootContextLoader::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class CountrySteps @Autowired constructor(world: World, countryApi: CountryApi) : En {
    init {
        Given("^the system has the following countries$") {
            countries: DataTable -> countries
                .asList(CountryModel::class.java)
                .forEach { countryApi.create(it) }
        }

        When("^the client requests to get all countries$") {
            world.response = countryApi.getAll()
        }

        Then("^the response contains the following countries$") {
            countries: DataTable ->
                assertThat(world.responseAsListOf<CountryModel>(), `is`(countries.asList(CountryModel::class.java)))
        }
    }
}