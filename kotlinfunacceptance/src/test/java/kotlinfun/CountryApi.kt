package kotlinfun

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpGet
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Component

@Component
class CountryApi @Autowired constructor(
        val world: World,
        val jdbcTemplate: JdbcTemplate,
        val httpClient: HttpClient,
        val objectMapper: ObjectMapper) {

    private var id = 1

    fun create(country: CountryModel) {
        val result = jdbcTemplate.update("""
            INSERT INTO COUNTRIES VALUES (
                ${id++},
                '${country.name}',
                ${country.population},
                '${country.language}',
                '${country.currency}')
        """)

        if (result == 1) {
            world.countries.plus(Pair(id, country))
        }
    }

    fun getAll(): List<CountryModel> = objectMapper
            .readValue<CountriesResponse>(httpClient.execute(HttpGet("http://localhost:8080/countries")).entity.content)
            .countries

    data class CountriesResponse(val countries: List<CountryModel>)
}